package fr.natsystem.eval.test;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.natsystem.eval.dao.impl.AgentDS;
import fr.natsystem.eval.dao.impl.BusinessUnitDS;
import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.BusinessUnit;




@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestAgentDAO {
	
	@Inject
	AgentDS dao;
	
	@Inject
	BusinessUnitDS bDAO;
	
//	@Test
//	public void testGetListUnmanagedAgentsParUnite(){
//		BusinessUnit unite1 = bDAO.getAllBusinessUnits().get(0);
//		System.out.println(dao.getListUnmanagedAgentsParUnite(unite1));
//	}
//
//	@Test
//	public void testGetListManagedAgentsParUnite(){
//		BusinessUnit unite1 = bDAO.getAllBusinessUnits().get(0);
//		System.out.println(dao.getListManagedAgentsParUnite(unite1));
//	}
//	
//	@Test
//	public void testGetListAgentsNonDirecteurParUnite(){
//		BusinessUnit unite1 = bDAO.getAllBusinessUnits().get(0);
//		System.out.println(dao.getListAgentsNonDirecteurParUnite(unite1));
//	}

	@Test
	public void testGetListDPXParUnite(){
		BusinessUnit unite1 = bDAO.getAllBusinessUnits().get(0);
		System.out.println(dao.getListDPXParUnite(unite1));
	}

	@Test
	public void testGetDUParUnite(){
		BusinessUnit unite1 = bDAO.getAllBusinessUnits().get(0);
		System.out.println(dao.getDUParUnite(unite1));
	}
	
	@Test
	public void testGetAgentsParUnite(){
		BusinessUnit unite1 = bDAO.getAllBusinessUnits().get(1);
		System.out.println(dao.getListeAgentsParUnite(unite1));
	}
	
	@test
	public void testAgentConnection(){
		Agent agent = dao.getByCP("6512471Y");
		
	}
	
//	@Test
//	public void testGetListManagedAgentsParUniteNommee(){
//		List<BusinessUnit> allBusinessUnits = bDAO.getBusinessUnitParNom("PTF Lorraine");
//		BusinessUnit unite1 = allBusinessUnits.get(0);
//		Agent du = dao.getDUParUnite(unite1).get(0);
//		System.out.println(du.getNumCP());
//		System.out.println(dao.getListManagedAgentsParUnite(unite1));
//	}
}
