package fr.natsystem.eval.test;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.natsystem.eval.dao.impl.AgentDS;
import fr.natsystem.eval.dao.impl.PointCleDAO;
import fr.natsystem.eval.dao.impl.SAMIDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/applicationContext.xml"})
public class TestCreationEvalInitiales {

	@Inject
	PointCleDAO pointCleDAO;
	
	@Inject
	AgentDS agentDAO;
	
	@Inject
	SAMIDAO samiDAO;
	
	@Test
	public void creationEvaluationsInitiales() {
//		List<PointCle> listePointCles = pointCleDAO.getAllPointsCles();
//		List<Agent> listeAgents = agentDAO.getListeAgents();
//		int i = 0;
//		List<EvaluationInitiale> listEval = new ArrayList<EvaluationInitiale>();
//		for (Agent agent : listeAgents) {
//			for (PointCle pointCle : listePointCles) {
//				SAMI sami;
//				if (i == 0) {
//					sami = samiDAO.getById(1);
//					i++;
//				}
//				else if (i == 1) {
//					sami = samiDAO.getById(2);
//					i++;
//				}
//				else if (i == 2) {
//					sami = samiDAO.getById(3);
//					i++;
//				}
//				else  {
//					sami = samiDAO.getById(4);
//					i = 0;
//				}
//				SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//				Date date = null;
//				try {
//					date = formatter.parse("2012/10/11");
//				} catch (ParseException e) {
//				}
//				EvaluationInitiale eval = new EvaluationInitiale(pointCle, agent, date, sami, true);
//				listEval.add(eval);
//			}
//		}
//		
//		NatOrbSession session = NatOrbUtil.getSession();
//        NatTransaction transaction = session.beginTransaction();
//
//        for (EvaluationInitiale eval : listEval) {
//        	session.saveOrUpdate(eval);
//        }
//        transaction.commit();
	}
}
