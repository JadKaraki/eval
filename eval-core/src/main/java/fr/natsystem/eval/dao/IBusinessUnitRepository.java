/**
 * 
 */
package fr.natsystem.eval.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.natsystem.eval.datamodel.BusinessUnit;

@Repository
public interface IBusinessUnitRepository extends JpaRepository<BusinessUnit, Long>{


}
