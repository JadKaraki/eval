package fr.natsystem.eval.dao.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import fr.natsystem.eval.dao.IAgentDAO;
import fr.natsystem.eval.dao.IAgentRepository;
import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.BusinessUnit;



@Component
public class AgentDS implements IAgentDAO{
	
	@Inject
	IAgentRepository repository;
	

	@Inject
	@Named("request_agent_dpx_unite")
	String request_agent_dpx_unite;
	
	@Inject
	@Named("request_agent_du_unite")
	String request_agent_du_unite;
	
	@Inject
	@Named("request_agent_non_d_unite")
	String request_agent_non_d_unite;
	
	@Inject
	@Named("request_unmanaged_agent_non_d_unite")
	String request_unmanaged_agent_non_d_unite;
	
	@Inject
	@Named(value="request_managed_agent_non_d_unite")
	String request_managed_agent_non_d_unite;
	
	@Override
	public List<Agent> getListeAgents() {
		return repository.findAll(new Sort("nom"));
	}
	
	public List<Agent> getListeAgentsParAgentRef(Agent agentRef) {
		return repository.findAgentsByAgentRef(agentRef, new Sort("nom"));
	}
	
	public List<Agent> getListeAgentsParUnite(BusinessUnit unite) {
		return repository.findAgentsByUnite(unite);
	}
	

//	public List<Agent> getListUnmanagedAgentsParUnite(BusinessUnit unite) {
//		Agent agent = new Agent();
//		agent.setUnite(unite);
//		WhereClauseBuilder wcb = new WhereClauseBuilder(agent);
//		wcb.setOrderBy("nom");
//		wcb.excludeProperty("prenomNom");
//		wcb.setPropertyFromManyToOne("unite", "id", WhereClauseBuilder.EQUAL);
//		String wc = wcb.getWhereClause();
//		wc += " and Agent.agentRef is null";
//		wcb.setStoredWhereClause(wc);
//		NatOrbQuery query = wcb.getQuery();
//		List<Agent> list = (List<Agent>) query.execute();
//		return list;
//	}
//
//	public List<Agent> getListManagedAgentsParUnite(BusinessUnit unite) {
//		Agent agent = new Agent();
//		agent.setUnite(unite);
//		WhereClauseBuilder wcb = new WhereClauseBuilder(agent);
//		wcb.setOrderBy("nom");
//		wcb.excludeProperty("prenomNom");
//		wcb.setPropertyFromManyToOne("unite", "id", WhereClauseBuilder.EQUAL);
//		String wc = wcb.getWhereClause();
//		wc += " and Agent.agentRef is not null";
//		wcb.setStoredWhereClause(wc);
//		NatOrbQuery query = wcb.getQuery();
//		List<Agent> list = (List<Agent>) query.execute();
//		return list;
//	}
//	
//	public List<Agent> getListUnmanagedAgentsNonDirecteurParUnite(BusinessUnit unite) {
//		final Session session = (Session) this.getCurrentSession().getNativeSession();
//		final Query query = session.createQuery(request_unmanaged_agent_non_d_unite);
//		query.setParameter("unite", unite);
//		List<Agent> list = (List<Agent>) query.list();
//		return list;
//	}
//	
//	public List<Agent> getListManagedAgentsNonDirecteurParUnite(BusinessUnit unite) {
//		final Session session = (Session) this.getCurrentSession().getNativeSession();
//		final Query query = session.createQuery(request_managed_agent_non_d_unite);
//		query.setParameter("unite", unite);
//		List<Agent> list = (List<Agent>) query.list();
//		return list;
//	}
	
//	public List<Agent> getListAgentsNonDirecteurParUnite(BusinessUnit unite) {
//		final Session session = (Session) this.getCurrentSession().getNativeSession();
//		final Query query = session.createQuery(request_agent_non_d_unite);
//		query.setParameter("unite", unite);
//		List<Agent> list = (List<Agent>) query.list();
//		return list;
//	}

	public List<Agent> getListDPXParUnite(BusinessUnit unite) {
		return this.repository.findAgentsWithRoleAndBusinessUnit(unite, "DPX");
	}
	
	public List<Agent> getDUParUnite(BusinessUnit unite) {
		return this.repository.findAgentsWithRoleAndBusinessUnit(unite, "DU");
	}
	
	
	public void deleteAgent(Agent entity){
		this.repository.delete(entity);
	}

	public void saveAgent(Agent entity) {
		this.repository.save(entity);
	}

	public Agent getByCP(String login) {
		List<Agent> list = this.repository.findAgentsByNumCP(login);
		return list.get(0);
		//Agent agent = new Agent();
		//agent.setNumCP(login);
		//return this.repository.findOne(Example.of(agent));
	}

	
	
//	public boolean compareDPXAgent(Agent agent){
//		if (agent == null) {
//			return false;
//		}
//		final Session session = (Session) this.getCurrentSession().getNativeSession();
//		final Query query = session.createSQLQuery("select agent_ref from Agent where agent_id = :id").setInteger("id", agent.getId());
//		Integer dbAgentRef = (Integer) query.uniqueResult();
//		
//		Agent agentRef = agent.getAgentRef();
//		if (agentRef == null && dbAgentRef == null) {
//			return true;
//		} else if(agentRef != null && dbAgentRef != null) {
//			if (agentRef.getId() == dbAgentRef) {
//				return true;
//			} else {
//				return false;
//			}
//		} else {
//			return false;
//		}
//	}
	
	

	public void saveAgentNoTx(Agent agent) {
		this.repository.save(agent);
	}
	
	public void saveListeAgents(List<Agent> listeAgents) {
		this.repository.save(listeAgents);
	}


}
