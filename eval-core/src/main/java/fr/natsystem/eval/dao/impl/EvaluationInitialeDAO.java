package fr.natsystem.eval.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.natsystem.eval.datamodel.Agent;
import fr.natsystem.eval.datamodel.EvaluationInitiale;
import fr.natsystem.eval.datamodel.PointCle;

@Component
public class EvaluationInitialeDAO{

	public EvaluationInitiale getEvaluationInitParPCAgentDate(PointCle pointCle, Agent agent, Date date) {
		// TODO complete
		return null;
	}
	
	public EvaluationInitiale getLastEvaluationInitParPCAgent(PointCle pointCle, Agent agent) {
		// TODO complete
		return null;
	}

	/**
	 * 
	 * @param pointCle
	 * @param agent
	 * @param numberOfEval
	 * @return
	 */
	public List<EvaluationInitiale> getListLastEvaluationInitParPCAgent(PointCle pointCle, Agent agent, int numberOfEval) {
		// TODO complete
		return null;
	}
	
	public List<EvaluationInitiale> getEvaluationsPourAgentsEtPointsCles(List<Agent> listeAgents, List<PointCle> listePointsCles) {
		// TODO complete
		return null;
	}
	
	
	/**
	 * Obtenir la liste des évaluations initiales pour une liste d'agent
	 * @param listeAgents
	 * @return
	 */
	public List<EvaluationInitiale> getEvaluationsPourAgents(List<Agent> listeAgents) {
		// TODO complete
		return null;
	}
	
	/**
	 * Récupération de l'historique pour les 3 dernières années
	 * @param agent
	 * @param pointCle
	 * @return
	 */
	public List<EvaluationInitiale> getEvaluationsParAgentPCDes3DernieresAnnees(Agent agent, PointCle pointCle) {
		// TODO complete
		return null;
	}


	

}
