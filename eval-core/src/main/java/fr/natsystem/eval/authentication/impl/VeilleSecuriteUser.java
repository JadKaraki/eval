package fr.natsystem.eval.authentication.impl;

import fr.natsystem.eval.authentication.IUser;
import fr.natsystem.eval.authorizations.IRole;
import fr.natsystem.eval.datamodel.Agent;

public class VeilleSecuriteUser implements IUser {
	

	private String mail;

	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	private IRole role;
	private Agent agent;

	public VeilleSecuriteUser() {
		// TODO Auto-generated constructor stub
	}


	@Override
	public String getDisplayedName() {
		if (this.agent == null){
			return "";
		}
		return this.agent.getPrenom() +  " " +this.agent.getNom();

	}

	@Override
	public IRole getRole() {
		// TODO Auto-generated method stub
		return this.role;
	}
	
	@Override
	public Agent getAgent() {
		return this.agent;
	}


	public void setRole(IRole role) {
		this.role = role;
	}


	public void setAgent(Agent agent) {
		this.agent = agent;
	}

}
