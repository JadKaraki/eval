package fr.natsystem.eval.authentication;

import fr.natsystem.eval.authorizations.IRole;
import fr.natsystem.eval.datamodel.Agent;

public interface IUser {
	
	public String getDisplayedName();
	public IRole getRole();
	public Agent getAgent();
	
}
