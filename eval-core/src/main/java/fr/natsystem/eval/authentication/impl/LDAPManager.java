//package fr.natsystem.eval.authentication.impl;
//
//import java.util.List;
//
//import javax.naming.directory.Attributes;
//import javax.naming.directory.BasicAttribute;
//import javax.naming.directory.BasicAttributes;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ldap.core.LdapTemplate;
//import org.springframework.security.ldap.userdetails.Person;
//
//import fr.natsystem.eval.authentication.IAuthenticator;
//import fr.natsystem.eval.authentication.IUser;
//import fr.natsystem.eval.datamodel.Agent;
//import fr.natsystem.eval.datamodel.Role;
//
///**
// * @author Nat System Date: 09/02/14
// */
//public class LDAPManager implements IAuthenticator {
//
//	@Inject
//	private AgentDAO agentDAO;
//
//	@Inject
//	private HabilitationDAO habDAO;
//
//	private String host;
//	private Integer port;
//
//	private String rootDn;
//	private String rootPwd;
//
//	private String baseDN;
//
//	@Inject
//	private LdapTemplate ldapTemplate;
//
//	public LDAPManager() {
//		this.ldapTemplate = (LdapTemplate) NsConfig
//				.getSpringBean("ldapTemplate");
//	}
//
//	public String getHost() {
//		return host;
//	}
//
//	public void setHost(String host) {
//		this.host = host;
//	}
//
//	public void setRootPwd(String rootPwd) {
//		this.rootPwd = rootPwd;
//	}
//
//	public void setPort(Integer port) {
//		this.port = port;
//	}
//
//	public String getBaseDN() {
//		return baseDN;
//	}
//
//	public void setBaseDN(String baseDN) {
//		this.baseDN = baseDN;
//	}
//
//	public void setRootDn(String rootDn) {
//		this.rootDn = rootDn;
//	}
//
//	public IUser authenticate(String user, String password) {
//		String keyStorePath = (String) NsConfig.getSpringBean("pathToKeyStore");
//		System.setProperty("javax.net.ssl.trustStore", keyStorePath);
//		this.ldapTemplate = (LdapTemplate) NsConfig
//				.getSpringBean("ldapTemplate");
//		boolean isAuthenticated = this.ldapTemplate.authenticate(this.baseDN,
//				"cn=" + user, password);
//		if (!isAuthenticated) {
//			return null;
//		}
//		List<IUser> users = this.ldapTemplate.search(this.baseDN, "cn=" + user,
//				new UserAttributesMapping());
//		VeilleSecuriteUser vsu = (VeilleSecuriteUser) users.get(0);
//		Agent agent = agentDAO.getByCP(user);
//		if (agent == null) {
//			return null;
//		}
//		vsu.setAgent(agent);
//		List<Role> roles = habDAO.getAgentRoles(agent);
//		if (roles == null || roles.isEmpty()) {
//			return null;
//		}
//		vsu.setRole(roles.get(0));
//		return vsu;
//
//	}
////	public String resolveMailFromCodeCP(String codeCP) {
////		return "qdecayeux@natsystem.fr";
////
////	}
//	
//	public String resolveMailFromCodeCP(String codeCP) {
//		String mail = "";
//		List<VeilleSecuriteUser> users = this.ldapTemplate.search(this.baseDN,
//				"cn=" + codeCP, new UserAttributesMapping());
//		if (users == null || users.isEmpty()) {
//			return "";
//		}
//		return users.get(0).getMail();
//
//	}
//
//	private String getDnForUser(String uid) {
//		return "";
//	}
//
//	private Attributes buildAttributes(Person p) {
//		Attributes attrs = new BasicAttributes();
//		BasicAttribute ocattr = new BasicAttribute("objectclass");
//		ocattr.add("top");
//		ocattr.add("person");
//		attrs.put(ocattr);
//		attrs.put("cn", "Some Person");
//		attrs.put("sn", "Person");
//		return attrs;
//	}
//
//	public void resolveMailForUsers(List<VeilleSecuriteUser> users) {
//		for (VeilleSecuriteUser user : users) {
//			user.setMail(resolveMailFromCodeCP(user.getMail()));
//		}
//	}
//
//	// TODO replace actual method to perform only on ldap query instead of n
//	// public List<VeilleSecuriteUser>
//	// resolveMailsFromUsers(List<VeilleSecuriteUser> users){
//	// String mail = "";
//	// if (users == null){
//	// return null;
//	// }
//	// String ldapFilter = "(|";
//	// String unitFilterTemplate = "(cn=${userName})";
//	// for (VeilleSecuriteUser user : users){
//	// ldapFilter += unitFilterTemplate.replaceAll("${userName}",
//	// user.getAgent().getNumCP());
//	// }
//	// ldapFilter += ")";
//	// List<VeilleSecuriteUser> users = this.ldapTemplate.search(this.baseDN,
//	// "cn=" + codeCP, new UserAttributesMapping());
//	//
//	// return users.get(0).getMail();
//	//
//	// }
//
//}
