package fr.natsystem.eval.authentication;

public interface IAuthenticator {
	
	
	public IUser authenticate(String login, String pwd); 

	
	public String resolveMailFromCodeCP(String codeCP);
}
