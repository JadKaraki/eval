package fr.natsystem.eval.authorizations.nav;

/**
 * Holds the UI Information of a navigation group
 * 
 * @author NatSystem
 *
 */
public class NavigationPart {
	private String label;
	private String icon;
	private boolean mustSaveBeforeClose;

	public NavigationPart() {

	}

	public NavigationPart(String label, String icon) {
		super();
		this.label = label;
		this.icon = icon;
	}

	public NavigationPart(String label, String icon, boolean mustRecordBeforClose) {
		super();
		this.label = label;
		this.icon = icon;
		this.mustSaveBeforeClose = mustRecordBeforClose;
	}

	/**
	 * Get the displayed value
	 * 
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Get the displayed Icon Path
	 * 
	 * @return
	 */
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public boolean isMustSaveBeforeClose() {
		return mustSaveBeforeClose;
	}

	public void setMustSaveBeforeClose(boolean mustRecordBeforeClose) {
		this.mustSaveBeforeClose = mustRecordBeforeClose;
	}

}
