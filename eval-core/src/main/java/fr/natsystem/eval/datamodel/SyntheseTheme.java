package fr.natsystem.eval.datamodel;

import java.math.BigDecimal;

public class SyntheseTheme {

	private int numTheme;
	
	private String nomTheme;
	
	private double minimumScore;
	
	private BigDecimal observedScore;
	
	private double maximumScore;
	
	private SAMI evaluation;
	
	private BigDecimal relativeScore;
	
	private SAMI evaluationInitiale;

	public SyntheseTheme() {
		super();
	}

	public SyntheseTheme(int numTheme, String nomTheme, double minimumScore,
			BigDecimal observedScore, double maximumScore, SAMI evaluation,
			BigDecimal relativeScore, SAMI evaluationInitiale) {
		super();
		this.numTheme = numTheme;
		this.nomTheme = nomTheme;
		this.minimumScore = minimumScore;
		this.observedScore = observedScore;
		this.maximumScore = maximumScore;
		this.evaluation = evaluation;
		this.relativeScore = relativeScore;
		this.evaluationInitiale = evaluationInitiale;
	}

	public int getNumTheme() {
		return numTheme;
	}

	public void setNumTheme(int numTheme) {
		this.numTheme = numTheme;
	}

	public String getNomTheme() {
		return nomTheme;
	}

	public void setNomTheme(String nomTheme) {
		this.nomTheme = nomTheme;
	}

	public double getMinimumScore() {
		return minimumScore;
	}

	public void setMinimumScore(double minimumScore) {
		this.minimumScore = minimumScore;
	}

	public BigDecimal getObservedScore() {
		return observedScore;
	}

	public void setObservedScore(BigDecimal observedScore) {
		this.observedScore = observedScore;
	}

	public double getMaximumScore() {
		return maximumScore;
	}

	public void setMaximumScore(double maximumScore) {
		this.maximumScore = maximumScore;
	}

	public SAMI getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(SAMI evaluation) {
		this.evaluation = evaluation;
	}

	public BigDecimal getRelativeScore() {
		return relativeScore;
	}

	public void setRelativeScore(BigDecimal relativeScore) {
		this.relativeScore = relativeScore;
	}

	public SAMI getEvaluationInitiale() {
		return evaluationInitiale;
	}

	public void setEvaluationInitiale(SAMI evaluationInitiale) {
		this.evaluationInitiale = evaluationInitiale;
	}

	
}
