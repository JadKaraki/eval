package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="HABILITATIONS")
public class Habilitation implements Serializable{
	

	private static final long serialVersionUID = 5090858459204974468L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="HABILITATIONS_ID")
	private int id;

	@ManyToOne
	@JoinColumn(name="HABILITATION_AGENT")
	private Agent agent;
	
	@ManyToOne
	@JoinColumn(name="HABILITATION_ROLE")
	private Role role;
	
	public Habilitation() {
	}
	
	public Habilitation(Agent agent, Role role) {
		super();
		this.agent = agent;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	
	
	
	

}
