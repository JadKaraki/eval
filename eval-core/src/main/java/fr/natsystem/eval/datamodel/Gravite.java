package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GRAVITE")
public class Gravite implements Serializable{

	private static final long serialVersionUID = 5060037410095868709L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="GRAVITE_ID")
	private int id;
	
	@Column(name="GRAVITE_VALEUR")
	private int valeur;
	
	@Column(name="GRAVITE_LIBELLE")
	private String libelle;

	public Gravite() {
	}
	
	public Gravite(int valeur, String libelle) {
		super();
		this.valeur = valeur;
		this.libelle = libelle;
	}

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
