package fr.natsystem.eval.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DIRECTION_FRET")
public class DirectionFret {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="DIRECTION_FRET_ID")
	private int id;
	
	@Column(name="DIRECTION_FRET_LIBELLE")
	private String libelle;

	public DirectionFret() {
		super();
	}

	public DirectionFret(DirectionFret df) {
		this.id = df.getId();
		this.libelle = df.getLibelle();
	}
	
	public DirectionFret(String libelle) {
		super();
		this.libelle = libelle;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DirectionFret other = (DirectionFret) obj;
		if (id != other.id)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

	
	
}
