package fr.natsystem.eval.datamodel;

public enum EtatEval {

	INIT("INIT", "Non commencée"),
	EVALUE("EVALUE", "En cours de saisie"),
	SOUMIS("SOUMIS", "En attente de validation du DU"),
	VALIDE("VALIDE", "Validée"),
	REJETE("REJETE", "Rejetée");
	
	private String etatEval = "";
	private String etatWorkflow = "";
	
	EtatEval(String etatEval, String etatWorkflow) {
		this.etatEval = etatEval;
		this.etatWorkflow = etatWorkflow;
	}
	
	public String toString() {
		return etatEval;
	}
	
	public String getEtatWorkflow() {
		return etatWorkflow;
	}
	
}
