package fr.natsystem.eval.datamodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="BUSINESS_UNITS")
public class BusinessUnit implements Serializable{
	

	private static final long serialVersionUID = 3261835095798257247L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="BUSINESS_UNIT_ID")
	private int id;
	
	@Column(name="BUSINESS_UNIT_LIBELLE")
	private String libelle;

	@ManyToOne
	@JoinColumn(name="BUSINESS_UNIT_DF")
	private DirectionFret directionFretRef;
	
	public BusinessUnit() {
	}
	
	public BusinessUnit(String libelle, DirectionFret DF) {
		this.libelle = libelle;
		this.directionFretRef = DF;
	}
	
	public BusinessUnit(BusinessUnit unite) {
		this.id = unite.getId();
		this.libelle = unite.getLibelle();
		if (unite.getDirectionFretRef() != null) {
			DirectionFret df = new DirectionFret(unite.getDirectionFretRef());
			this.directionFretRef = df;
		}
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public DirectionFret getDirectionFretRef() {
		return directionFretRef;
	}

	public void setDirectionFretRef(DirectionFret directionFretRef) {
		this.directionFretRef = directionFretRef;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((directionFretRef == null) ? 0 : directionFretRef.hashCode());
		result = prime * result + id;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BusinessUnit other = (BusinessUnit) obj;
		if (directionFretRef == null) {
			if (other.directionFretRef != null)
				return false;
		} else if (!directionFretRef.equals(other.directionFretRef))
			return false;
		if (id != other.id)
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BusinessUnit [id=" + id + ", libelle=" + libelle + ", directionFretRef=" + directionFretRef + "]";
	}
	

	
}
