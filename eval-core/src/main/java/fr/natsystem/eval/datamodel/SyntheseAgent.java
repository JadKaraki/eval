package fr.natsystem.eval.datamodel;

import java.math.BigDecimal;


public class SyntheseAgent {

	private int numAgent;
	
	private String nomAgent;
	
	private CoefficientContexteAgent coeffContexteAgent;
	
	private double minimumScore;
	
	private BigDecimal observedScore;
	
	private double maximumScore;
	
	private SAMI evaluation;
	
	private BigDecimal relativeScore;
	
	private SAMI evaluationInitiale;

	public SyntheseAgent() {
		super();
	}

	public SyntheseAgent(int numAgent, String nomAgent, CoefficientContexteAgent coeffContexteAgent, double minimumScore, BigDecimal observedScore, double maximumScore,
			SAMI evaluation, BigDecimal relativeScore, SAMI evaluationInitiale) {
		this.setNumAgent(numAgent);
		this.setNomAgent(nomAgent);
		this.setCoeffContexteAgent(coeffContexteAgent);
		this.minimumScore = minimumScore;
		this.observedScore = observedScore;
		this.maximumScore = maximumScore;
		this.evaluation = evaluation;
		this.relativeScore = relativeScore;
		this.evaluationInitiale = evaluationInitiale;
	}

	public double getMinimumScore() {
		return minimumScore;
	}

	public void setMinimumScore(double minimumScore) {
		this.minimumScore = minimumScore;
	}

	public BigDecimal getObservedScore() {
		return observedScore;
	}

	public void setObservedScore(BigDecimal observedScore) {
		this.observedScore = observedScore;
	}

	public double getMaximumScore() {
		return maximumScore;
	}

	public void setMaximumScore(double maximumScore) {
		this.maximumScore = maximumScore;
	}

	public SAMI getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(SAMI evaluation) {
		this.evaluation = evaluation;
	}

	public BigDecimal getRelativeScore() {
		return relativeScore;
	}

	public void setRelativeScore(BigDecimal relativeScore) {
		this.relativeScore = relativeScore;
	}

	public String getNomAgent() {
		return nomAgent;
	}

	public void setNomAgent(String nomAgent) {
		this.nomAgent = nomAgent;
	}

	public SAMI getEvaluationInitiale() {
		return evaluationInitiale;
	}

	public void setEvaluationInitiale(SAMI evaluationInitiale) {
		this.evaluationInitiale = evaluationInitiale;
	}

	public CoefficientContexteAgent getCoeffContexteAgent() {
		return coeffContexteAgent;
	}

	public void setCoeffContexteAgent(CoefficientContexteAgent coeffContexteAgent) {
		this.coeffContexteAgent = coeffContexteAgent;
	}

	public int getNumAgent() {
		return numAgent;
	}

	public void setNumAgent(int numAgent) {
		this.numAgent = numAgent;
	}
	
}
